 import React from 'react';
 import { Route, Redirect, Switch } from 'react-router-dom'
 import { makeStyles } from '@material-ui/core/styles';

 import HomeView from '../views/Home/HomeView';
 import UserView from '../views/User/UserView';

const useStyles = makeStyles((theme) => ({
    root: {

    },
}));

const PageRoutes = (props) =>{

    const classes = useStyles();
    
    return( <Switch>
                <Route path="/" exact>
                    <HomeView />
                </Route>
                <Route path="/user" >
                    <UserView />
                </Route>
            </Switch>);

}
export default PageRoutes;