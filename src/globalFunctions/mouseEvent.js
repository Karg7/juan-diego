import { useEffect, useState } from "react";


export const useMousePosition = () => {
  const [position, setPosition] = useState({x:0, state:false});

  useEffect(() => {

    

   const myfun = e =>{
        let time
        setPosition({x: e.clientX, state:true})
        let px = position.x
        clearTimeout(time)
        time = setTimeout (()=>{
            if(position.x === px){
                setPosition({x: 10, state:false})
            }
        }, 7000)

   }

    window.addEventListener("mousemove", myfun);

    return () => {
      window.removeEventListener("mousemove", myfun );
    };
  }, []);

  return position;

    
};


/* const myfun = e =>{
    let time
    setPosition({x: e.clientX, state:true})
    let px = position.x
    clearTimeout(time)
    time = setTimeout (()=>{
        if(position.x === px){
            setPosition({x: 10, state:false})
        }
    }, 7000)
    
} */