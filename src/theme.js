import { createMuiTheme } from '@material-ui/core/styles';
//import blueGray from '@material-ui/core/colors/blueGrey';


const theme = createMuiTheme({
    palette: {
        primary: { main: '#009688' }, // Purple and green play nicely together. #091635
        secondary: { main: '#1B4965' }, // This is just green.A700 as hex. #00acc1

        //options for secondary
        //#0b3954
       
    },
    typography: {
        useNextVariants: true,
        fontFamily:[
        'Poppins',
        ].join(','),
    },
    /* overrides:{
        MuiTypography:{
            h4:{
                color: blueGrey[700],
            },
            h5:{
                color: blueGrey[700],
            },
            h6:{
                color: blueGrey[700],
            },
            subtitle1:{
                color: blueGrey[900],
                fontWeight:500
            },
            subtitle2:{
                color: blueGrey[900],
            },
            body1:{
                color: blueGrey[700],
            },
            body2:{
                color: blueGrey[700],
            },
            caption:{
                color: blueGrey[700],
            },
        }
    } */

});

export default theme;