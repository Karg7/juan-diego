import React , {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { BrowserRouter as Router } from "react-router-dom";

import ToolbarMine from '../components/Toolbar/Toolbar';
import HomeView from '../views/Home/HomeView';
import Footer from '../components/Footer/Footer';

//import {useMousePosition} from '../globalFunctions/mouseEvent';
import moment from 'moment';
import {mainServer} from '../variables/config';
import PageRoutes from '../routes/PageRoutes';

const useStyles = makeStyles((theme) => ({
    root: {
       // display: 'flex',
    },
    content:{
        flexGrow: 1,
        backgroundColor:'#e9ecf5'
        //backgroundColor:'#f4f8fb',
       //padding: theme.spacing(2),
    }
}));

const MainLayout = (props) =>{

    const classes = useStyles();
    const [notBusy, setNotBusy] = useState(true)

    /* const request_reauthenticate = async() => {
        console.log('hola')
        let response;
        response = await mainServer.instance.get('/login/reauthenticate')
        console.log(response)
        localStorage.setItem('localData',JSON.stringify(response.data.data));
    } */
    
   
    useEffect(() => {

        const myfun = async(e) =>{
            let temp = localStorage.getItem('localData');
            if(temp){
                temp = JSON.parse(temp);
                let currentTime = moment().unix();
                let tokenTime = temp.exp;
                //nuevo para obtener la info del usuario
                /* mainServer.instance.defaults.headers.common['Authorization'] = temp.token; 
                let dataUser
                dataUser = await mainServer.instance.get('/users/me')
                console.log(dataUser)
                localStorage.setItem('dataUser',JSON.stringify(dataUser.data.data.user_found)); */

                if(currentTime < tokenTime){
                    if(currentTime + 15*60 > tokenTime){
                        console.log('necesita reautenticación')
                        try{
                            mainServer.instance.defaults.headers.common['Authorization'] = temp.token;    
                            let response
                            response = await mainServer.instance.get('/login/reauthenticate')
                            //setNotBusy(false)
                            localStorage.setItem('localData',JSON.stringify(response.data.data));
                            /* if (local){
                                setNotBusy(true)
                                console.log(local)
                            } */
                            console.log(response)
                        }catch(error){
                            console.log(error);
                        }  
                    }else{
                        console.log('activo')
                    } 
                }else{
                    console.log('el tiempo de la sesion ha expirado')
                    //cerrar sesion
                    localStorage.removeItem('localData');
                }
            }else{
                console.log('no se ha logeado')
            }
        }

        window.addEventListener("mousemove", myfun);

        return () => {
        window.removeEventListener("mousemove", myfun );
        };
    }, []);


    return(
        <div className={classes.root}>
            <Router>
                <ToolbarMine />
                <main className={classes.content}>
                    <PageRoutes />
                    <Footer />
                </main>
                {/* <PageRoutes /> */}
            </Router>
        </div>
    );

}
export default MainLayout;


/* return(
    <div className={classes.root}>
        <Router>
            <ToolbarMine />
            <main className={classes.content}>
                <PageRoutes />
                <Footer />
            </main>
        </Router>
    </div>
); */

/* const myfun = async(e) =>{
    let temp = localStorage.getItem('localData');
    if(temp){
        temp = JSON.parse(temp);
        let currentTime = moment().unix();
        let tokenTime = temp.exp;
        if(currentTime < tokenTime){
            if(currentTime + 15*60 > tokenTime){
                console.log('necesita reautenticación')
                try{
                    mainServer.instance.defaults.headers.common['Authorization'] = temp.token;    
                    let response
                    response = await mainServer.instance.get('/login/reauthenticate')
                    //setNotBusy(false)
                    localStorage.setItem('localData',JSON.stringify(response.data.data));
                     if (local){ //esto esta comentado originalmente
                        setNotBusy(true)
                        console.log(local)
                    } 
                    console.log(response)
                }catch(error){
                    console.log(error);
                }  
            }else{
                console.log('activo')
            } 
        }else{
            console.log('el tiempo de la sesion ha expirado')
            //cerrar sesion
            localStorage.removeItem('localData');
        }
    }else{
        console.log('no se ha logeado')
    }
} */