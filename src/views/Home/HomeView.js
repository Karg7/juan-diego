import React from 'react';
import { scroller, Element , Link} from "react-scroll";
import { makeStyles } from '@material-ui/core/styles';

import News from './components/news/News';
import ContentForYou from './components/contentForYou/ContentForYou';
import Banner1 from './components/Banner1/Banner1';
import Banner2 from './components/Banner2/Banner2';
import Footer from '../../components/Footer/Footer';
import ToolbarMine from '../../components/Toolbar/Toolbar';


const useStyles = makeStyles((theme) => ({
    root: {

    },
    subroot:{
        width:'80%',
        height:'100vh',
        margin:'auto',
        paddingTop: 16,
    },
    content:{
        flexGrow: 1,
        backgroundColor:'#e9ecf5'
        //backgroundColor:'#f4f8fb',
       //padding: theme.spacing(2),
    }
}));

const HomeView = (props) =>{

    const classes = useStyles();

    /* const scrollTo1 = () =>  {
        scroller.scrollTo("test1", {
          duration: 1000,
          delay: 0,
          smooth: "easeInOutCubic"
        });
    } */

    /* const scrollTo = (section) =>  {
        scroller.scrollTo(section, {
          duration: 1000,
          delay: 0,
          smooth: "easeInOutCubic"
        });
    } */

    
    return(
       <div>
           {/* <ToolbarMine />
            <main className={classes.content}> */}
                <Element name="1" >
                    <Banner1 />
                </Element>
                <Element name="2" >
                    <Banner2 />
                </Element>
                <Element name="3" >
                    <ContentForYou /> 
                </Element>
                <Element name="4" >
                    <News />   
                </Element>
                {/* <Footer />      
            </main> */}
        </div> 
        
    );

}
export default HomeView;