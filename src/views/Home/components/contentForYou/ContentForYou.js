import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';


import CardContent from './components/Card2';

import image from '../../../../assets/cards/alimentos.jpg';
import acne from '../../../../assets/cards/acnejpg.jpg';
import vitamina from '../../../../assets/cards/vitamina.jpg';
import aliento from '../../../../assets/cards/mal-aliento.jpg';

const useStyles = makeStyles((theme) => ({
    root: {
        //padding:'100px 0px',
        backgroundColor:'#fff',
        height:'100%'
        //backgroundColor:'#E2E8DD'
        //E3E7D3
        //FIF5F2 me gusto mucho pero esta muy clarito
        //e9ecf5 tambien me gusto
    },
    subroot:{
        width:'80%',
        margin:'auto',
        height:'100vh'
    },
    extraroot:{
        paddingTop:90
    }
}));

const ContentForYou = (props) =>{

    const classes = useStyles();

    const contents = [{title:'Problemas de piel relacionados a la ansidedad', img:image},
                      {title:'Lactancia reduce el riesgo de muerte subita', img:aliento},
                      {title:'Problemas de piel relacionados a la ansidedad', img:vitamina},
                      {title:'Problemas de piel relacionados a la ansidedad', img:acne},]
    
    return(
        <div className={classes.root}>
            <div className={classes.subroot}>
                <div className={classes.extraroot}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography variant='h5' color='primary'>Contenido para tí</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container spacing={4}>
                            {contents.map((item, index)=>{
                                return(
                                    <Grid item xs key={index} >
                                        <CardContent {...item} />
                                    </Grid>
                                )          
                            })}
                        </Grid>
                    </Grid>
                </Grid>
                </div>
            </div>
        </div>
    );

}
export default ContentForYou;