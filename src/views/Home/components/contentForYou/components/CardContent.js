 import React from 'react';
 import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import SimpleCard from '../../../../../components/Cards/SimpleCard';
import { amber, teal } from '@material-ui/core/colors';


const useStyles = makeStyles((theme) => ({
    root: {
        //margin:16,
        //width:300,
        
    },
    subroot:{
        //padding:20,
       // backgroundColor: teal[500],
       // color:'white',
        borderRadius:15
    },
    simpleCard:{
        
    },
    title:{
        width:'100%',
        //color:teal[900]
        //textAlign:'center',
    },
    imgContainer:{
        width:'100%',
        height:200,
        backgroundColor:'white',
        borderRadius:'15px 15px 0px 0px',
    },
    titleContainer:{
        margin:'8px 8px'
    }
}));

const CardContent = (props) =>{

    const classes = useStyles();

    const {title, img} = props;
    
    return(
        <div className={classes.root}>
           <SimpleCard>
                <div className={classes.subroot}>
                    <Grid container>
                        <Grid item xs={12}>
                            <img className={classes.imgContainer} src={img} alt=" " />
                        </Grid>
                        <Grid item>
                            <div className={classes.titleContainer}>
                            <Typography className={classes.title}>{title}</Typography>
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </SimpleCard>
        </div>
    );

}
export default CardContent;