 import React from 'react';
 import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {

    },
    button:{
        backgroundColor:theme.palette.primary.main,
        color:'white',
        padding:'8px 20px',
        borderRadius:40,
        margin:'20px 0px'
    },
    imgContainer:{
        width:'100%',
        height:200,
        backgroundColor:'white',
        borderRadius:15,
    },
}));

const Card2 = (props) =>{

    const classes = useStyles();

    const {title, img} = props;
    
    return(
        <div className={classes.root}>
                <div className={classes.subroot}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <img className={classes.imgContainer} src={img} alt=" " />
                        </Grid>
                        <Grid item xs={12}>         
                            <Typography color='secondary' variant='h6'>Titulo</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography color='textSecondary' variant='body2'>
                            Lorem ipsum dolor sit amet consectetur adipiscing elit, sapien posuere taciti viverra consequat porta netus in, 
                            tortor rutrum ullamcorper commodo nullam himenaeos. 
                            Hac ultricies felis phasellus ante sociosqu molestie vivamus varius tempor at pharetra augue aenean euismod ac,
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Button className={classes.button} size='small'>Leer más</Button>
                        </Grid>
                    </Grid>
                </div>
        </div>
    );

}
export default Card2;