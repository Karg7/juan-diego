 import React from 'react';
 import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid, Typography } from '@material-ui/core';

import img from '../../../../assets/banners/sal.jpg'
import SimpleCard from '../../../../components/Cards/SimpleCard';
import { amber, red } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
    root: {
        width:'100%',
        height:'100%',
        //padding:'10px 0px'
        //backgroundColor:theme.palette.primary.main,
        //padding:'100px 0px'
    },
    subroot:{
        width:'80%',
        margin:'auto',
        height:'100vh',
    },
    extraroot:{
        paddingTop:150,
    },
    img:{
        width:'100%',
        height:'100%',
        backgroundColor:theme.palette.primary.main,
        borderRadius:15
    },
    content:{
        margin:'32px 0px',
        fontSize:'18px'
    },
    title:{
        //color: amber[500]
        color:theme.palette.secondary.main
    },
    mainInfo:{
      position:'absolute',
      top:'50%',
      left:'5%',
      transform:'translate(0%,-50%)',
    },
    container:{
        position:'relative',
        width:'100%',
        height:'100%'
    }
}));

const Banner2 = (props) =>{

    const classes = useStyles();

    const info = [{title:'¿Te preocupa que estes comiendo demasiada sal?',
                   content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna'+ 
                             'aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'+
                             'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'}]
    
    return(
        <div className={classes.root}>
            
                <div className={classes.subroot}>
                <div className={classes.extraroot}>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <img className={classes.img} src={img}/>
                    </Grid>
                    <Grid item xs={6}>
                        <div className={classes.container}>
                            <div className={classes.mainInfo}>
                            <Typography variant='h4' className={classes.title}>{info[0].title}</Typography>
                            <Typography className={classes.content} color='textSecondary'>
                                {info[0].content.slice(0,240)+'...'}
                            </Typography>
                            <Button size="small" color='primary' >Ver más</Button>
                            </div>
                        </div>
                    </Grid>
                </Grid>
                </div>
                </div>
            
        </div>
    );

}
export default Banner2;