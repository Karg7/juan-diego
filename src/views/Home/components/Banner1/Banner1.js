import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import banner1 from '../../../../assets/banners/doctor.jpg';

import Ask from './components/Ask';
import NavSections from './components/NavSections';
import Login from '../../../../components/Modals/Login';

const useStyles = makeStyles((theme) => ({
    root: {
        //position:'relative',
        width:'100%',
        height: '100vh',
    },
    imgContainer:{
        width:'100%',
        position:'relative',
        //vh para que conserve la altura de la foto
        //height:'100vh',
        height:500,
        //paddingTop:'20%',
        //backgroundColor:'red',
        borderRadius: 30,
        [theme.breakpoints.down('md')]: {
           //height:'30%',
           height:'30vh'
          },
    },
    img:{
        width:'100%',
        height:'100vh',
        //position:'absolute',
        //paddingTop:'20%',
        //usar esto cuando usamos vh para que la imagen
        //no se distorsione
        objectFit: 'cover',
        objectPosition: 'center',
        //borderRadius: 30,
    },
    info:{
        position: 'absolute',
        zIndex: 2,
        left: '10%',
        top: '50%',
        [theme.breakpoints.down('md')]: {
            top: '50%',
            left:40,
            //backgroundColor:'red'
        },
        [theme.breakpoints.down('sm')]:{
            top:'20%',
            left:'50%',
            //backgroundColor:'green'
        },
        [theme.breakpoints.down('xs')]:{
            top:'20%',
            left:'20%',
            //backgroundColor:'blue'
        }
    },
    sections:{
        position: 'absolute',
        zIndex: 2,
        left: '83%',
        top: '40%',
    }
}));

const Banner1 = (props) =>{

    const classes = useStyles();
    
    
    
    return(
        <div className={classes.root}>
            <div className={classes.imgContainer}>
                <img className={classes.img} src={banner1} alt="Banner" /> 
                <div className={classes.info}>
                    <Ask />
                </div>
                <div className={classes.sections}>
                    <NavSections />
                </div>
                
            </div>  
        </div>
    );
    

}
export default Banner1;