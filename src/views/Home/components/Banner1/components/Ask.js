 import React from 'react';
 import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid, InputBase, Typography } from '@material-ui/core';
import { teal } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
    root: {
        margin:20
    },
    button:{
        backgroundColor: teal[500],
        borderRadius:40,
        padding:'8px 20px',
        color:'white'
        
    }
}));

const Ask = (props) =>{

    const classes = useStyles();

    
    return(
        <div className={classes.root}>
        <Grid container spacing={2} >
            <Grid item xs={12}>
                <Typography variant='h5' color='primary'>¡Cuentamelo todo!</Typography>
            </Grid>
            <Grid item xs={12}>
                <InputBase
                placeholder="Escribeme tus dudas, yo te explico!"
                fullWidth
                inputProps={{ 'aria-label': 'search' }}
                />
            </Grid>
            <Grid item xs={12}>
                <Button className={classes.button}>Preguntar</Button>
            </Grid>
        </Grid>
        </div>
    );

}
export default Ask;