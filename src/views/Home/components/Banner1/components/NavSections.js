import React, {useState} from 'react';
import {Link } from 'react-scroll';

import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        width:220,
        position:'fixed',
        backgroundColor:'rgb(225, 225, 225, 0.5)',
        borderRadius:15,
        padding:'16px 8px 16px 16px'
    },
    dot:{
        width:16,
        height:16,
        //backgroundColor:'white',
        border:'1px solid #009688',
        borderRadius:'50%',
        margin:'8px 0px 8px 8px',
        //marginLeft:4
    },
    dotActive:{
        width:16,
        height:16,
        backgroundColor:theme.palette.primary.main,
        //border:'1px solid #009688',
        borderRadius:'50%',
        margin:'8px 0px 8px 8px',
    },
    active:{
        color:theme.palette.primary.main
    },
    link:{
        '&:hover':{
            cursor: 'pointer'
        },
        textAlign:'end'
    }
}));

const NavSections = (props) =>{

    const classes = useStyles();

    const [active, setActive]= useState(' ')

    //console.log(active)

    //const scroll = props.scroll

    const options = [{element:'1', label: 'Inicio'},
                     {element:'2', label: 'Información'}, 
                     {element:'3', label: 'Contenido para tí'}, 
                     {element:'4', label: 'Noticias de la semana'},]
    
    const handleSetActive = (index) =>{
        if (active !== index) {
            setActive(index)
        }
        console.log(active)
    }

    

    return(
        <div className={classes.root}>
            <Grid container>
                {options.map((item,index)=>{
                    return(
                        <Grid container key={index}>
                        <Grid item xs={10} >
                            <Link 
                            className={classes.link}
                            //activeClass={classes.active}
                            to={item.element}
                            spy={true}
                            //hashSpy={true}
                            smooth={true}
                            offset={10}
                            duration={800}
                            onSetActive={handleSetActive}
                            //onSetInactive={handleSetInactive}
                            >
                           <Typography>{item.label}</Typography>
                            </Link>
                        </Grid> 
                        <Grid item xs={2}><div className={active === item.element ? classes.dotActive : classes.dot}></div></Grid>
                        </Grid>
                    )
                })}
                {/* <Grid item xs={12}>
                    <Grid container>
                        <Grid item xs={10}><Button onClick={()=>{scroll('test1')}}>Información</Button></Grid>
                        <Grid item xs={2}><div className={classes.dot}></div></Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container>
                        <Grid item xs={10}><Button onClick={()=>{scroll('test2')}}>Contenido para tí</Button></Grid>
                        <Grid item xs={2}><div className={classes.dot}></div></Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container>
                        <Grid item xs={10}><Button onClick={()=>{scroll('test3')}}>Noticias de la semana</Button></Grid>
                        <Grid item xs={2}><div className={classes.dot}></div></Grid>
                    </Grid>
                </Grid> */}
            </Grid>
        </div>
    );

}
export default NavSections;