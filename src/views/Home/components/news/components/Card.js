import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import { teal } from '@material-ui/core/colors';


import SimpleCard from '../../../../../components/Cards/SimpleCard';

const useStyles = makeStyles((theme) => ({
    root:{
        width:700
    },
    subroot: {
        padding:20,
        //width:'100%',
    },
    content:{
       // width:120
    },
    title:{
        color: teal[900]
    }
}));

const CardNew = (props) =>{

    const classes = useStyles();

    const {title, img, content} = props;
    
    return(
        <div className={classes.root}>
        <SimpleCard>
            <div className={classes.subroot}>
            <Grid container>
                <Grid item xs={3}>
                    <div>imagen</div>
                </Grid>
                <Grid item xs={9}>
                    <Grid container>
                        <Grid item><Typography className={classes.title} >{title}</Typography></Grid>
                        <Grid item><Typography>{content}</Typography></Grid>
                    </Grid>
                </Grid>
            </Grid>
            </div>
        </SimpleCard>
        </div>
    );

}
export default CardNew;