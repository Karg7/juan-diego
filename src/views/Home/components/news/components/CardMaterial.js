import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Icon } from '@material-ui/core';

const useStyles = makeStyles({
  responsive:{
    position:'relative',
    width:'100%',
    height:'100vh',
    //paddingTop:'50%'
  },
  root: {
    minWidth: 288,
    height:400,
    width:'100%',
    //paddingTop:'133%',
    borderRadius:15,
    //marginBottom:100,
    //backgroundColor:'#e9ecf5'
    position:'relative'
  },
  media: {
    height: 140,
    borderRadius:15
  },
  actions:{
    position:'absolute',
    left:0,
    bottom:4
  },
  
});

export default function MediaCard(props) {
  const classes = useStyles();

  let {title, img, content} = props;

  /* if(title.length > 50){
    title = title.slice(0,50)+'...';
  } */

  if(content.length >100){
    content = content.slice(0,100)+'...'
  }

  return (
    <div className={classes.responsive} >
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={img}
          title="Contemplative Reptile"
        />
        <CardContent className={classes.title}>
          <Typography gutterBottom variant="h6" component="h2" color='secondary'>
            {title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {content}
          </Typography>
        </CardContent>
      </CardActionArea>
      <div className={classes.actions}>
      <CardActions>
        <Button size="small" color="primary">
          <Icon>share</Icon>
        </Button>
        <Button size="small" color="primary">
          ver más
        </Button>
      </CardActions>
      </div>
    </Card>
    </div>
  );
}