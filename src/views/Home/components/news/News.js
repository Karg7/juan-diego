import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {  Button, Grid, Icon, Typography, withStyles } from '@material-ui/core';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';


import CardNew from './components/CardMaterial';

import ejercicio from '../../../../assets/cards/ejercicio.jpeg';
import parpados from '../../../../assets/cards/parpados.jpg';
import hierro from '../../../../assets/cards/hierro.jpg';
import jugo from '../../../../assets/cards/jugo.jpg';
import depresion from '../../../../assets/cards/depresion.jpg';
import alchol from '../../../../assets/cards/alchol.jpg';

/* const MyCarousel = withStyles({
    root:{
        backgroundColor:'red'
    },
    controlDots:{
        backgroundColor:'red',
        color:'green',
        margin:100
    }
  })(Carousel); */

const useStyles = makeStyles((theme) => ({
    root: {
        //margin:'100px 0px'
        width:'80%',
        margin:'auto',
        height:'100vh',
    },
    subroot:{
        paddingTop:100,
    },
    div:{
        width:'100%',
        height:'60vh',
        backgroundColor:'#e9ecf5',
        //backgroundColor:'red'
        //paddingBottom:100
    },
    arrows:{
        textAlign:'end'
    },
    carousel:{
        '&:controlDots':{
            backgroundColor:theme.palette.secondary.main,
          }
    },
    dots:{
        width:10,
        height:10,
        backgroundColor:theme.palette.primary.main,
        borderRadius:'50%'
    },
    dotActive:{
        width:10,
        height:10,
        backgroundColor:theme.palette.secondary.main,
        borderRadius:'50%'
    }

}));

const News = (props) =>{

    const classes = useStyles();

    const news = [{title: 'Mujeres con mayor riesgo de cancer de mama', img: ejercicio, 
                   content: 'se sabe que cada una de ocho muejres desarrolara sñdfkñf lksdñflasñfd ksaklfsñaf lk alskfñasfjla kasldfasfl ksdfañ sfasdlj hola'},
                    {title: 'Aumenta tu consumo de hierro en la dieta', img: hierro, 
                    content: 'se sabe que cada una de ocho muejres desarrolara....'},
                    {title: 'Hombres: el sobrepeso podría prevenir la Artritis', img: jugo, 
                    content: 'se sabe que cada una de ocho muejres desarrolara....'},
                    {title: 'La ingesta moderada de alcohol puede mantener la salud mental', img: alchol, 
                    content: 'se sabe que cada una de ocho muejres desarrolara sdñkflsad lkjñlsadf ldkajfl kjñlsakfñsal  lkñdslfaj ingesta'},
                    {title: 'Cirugía estética de párpados alivia migraña', img: parpados, 
                    content: 'se sabe que cada una de ocho muejres desarrolara....'},
                    {title: 'Consumo alto de azucar aumenta el riesgo de depresion', img: depresion, 
                    content: 'se sabe que cada una de ocho muejres desarrolara....'},
                    {title: 'Jugo verde con arandano', img: jugo, 
                    content: 'se sabe que cada una de ocho muejres desarrolara....'}];

    
    const [currentSlide, setCurrentSlide] = React.useState(0);

    const next = () => {
        setCurrentSlide(currentSlide + 1)
        //console.log(currentSlide);
    };

    const prev = () => {
        setCurrentSlide(currentSlide - 1)
    };

    const updateCurrentSlide = (index) => {
        if (currentSlide !== index) {
            setCurrentSlide(index)
        }
    };

    const dots=[0,1,2];
    
    return(
        <div className={classes.root}>
            <div className={classes.subroot}>
                <Grid container spacing={2} >
                    <Grid item xs={12}>
                        <Grid container>
                            <Grid item md={10} xs={8}>
                                <Typography variant='h5' color='primary'>Noticias de la semana</Typography>
                            </Grid>
                            <Grid item md={2} xs={4}>
                                <div className={classes.arrows}>
                                <Button onClick={prev}><Icon color='primary' fontSize='large'>chevron_left</Icon></Button>
                                <Button onClick={next}><Icon color='primary' fontSize='large'>chevron_right</Icon></Button>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                    <Carousel 
                    autoPlay
                    selectedItem={currentSlide}
                    onChange={updateCurrentSlide}
                    showThumbs={false} 
                    infiniteLoop 
                    showStatus={false}
                    showArrows={false}
                    showIndicators={false}
                    
                    >
                        <div className={classes.div}>
                            <Grid container justify='center' spacing={3}>
                                <Grid item xs={12} sm={6} md={4}><CardNew {...news[0]} /></Grid>
                                <Grid item xs={12} sm={6} md={4}><CardNew {...news[1]} /></Grid>
                                <Grid item xs={12} sm={6} md={4}><CardNew {...news[2]} /></Grid>
                            </Grid> 
                        </div>
                        <div className={classes.div}>
                            <Grid container justify='center' spacing={3} >
                                <Grid item xs={12} sm={6} md><CardNew {...news[3]} /></Grid>
                                <Grid item xs={12} sm={6} md><CardNew {...news[4]} /></Grid>
                                <Grid item xs={12} sm={6} md><CardNew {...news[5]} /></Grid>
                            </Grid> 
                        </div>
                        <div className={classes.div}>
                            <Grid container justify='center' spacing={3}>
                                <Grid item xs={12} sm={6} md={4}><CardNew {...news[6]} /></Grid>
                                <Grid item xs={12} sm={6} md={4}><CardNew {...news[5]} /></Grid>
                                <Grid item xs={12} sm={6} md={4}><CardNew {...news[0]} /></Grid>
                            </Grid>
                        </div>
                    </Carousel>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container spacing={2} justify='center'>
                            {dots.map((item, index)=>{
                                if (index == currentSlide){
                                    return (
                                    <Grid item key={index}>
                                        <div className={classes.dotActive}/>
                                    </Grid>
                                    )
                                }
                                return(
                                    <Grid item key={index}>
                                        <div className={classes.dots}/>
                                    </Grid>
                                )
                            })}
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        </div>
    );

}
export default News;