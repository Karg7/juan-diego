 import React, {useState, useEffect} from 'react';

 //redux
 import {connect} from 'react-redux'
 import * as actionTypes from '../../store/actions';

 import {mainServer} from '../../variables/config';
 import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';

import ToolbarUser from '../../components/ToolbarUser/ToolbarUser'
import Footer from '../../components/Footer/Footer';
import SearchBar from '../../components/Toolbar/components/SearchBar';
import UserOp from './components/UserOp';
import UserData from './components/UserData';

const useStyles = makeStyles((theme) => ({
    root: {
        color:theme.palette.primary.main
    },
    subroot:{
        width:'80%',
        height:'100 %',
        margin:'auto',
        padding:'100px 0px'
    },
    data:{
        padding:12,

    }
}));

const UserView = (props) =>{

    const classes = useStyles();

    const {onUpdateUser, isUserLogged} = props

    useEffect(async()=>{
        let temp = localStorage.getItem('localData');
        if(temp){
            temp = JSON.parse(temp);
            mainServer.instance.defaults.headers.common['Authorization'] = temp.token; 
            let dataUser
            dataUser = await mainServer.instance.get('/users/me')
            console.log(dataUser)
            //localStorage.setItem('dataUser',JSON.stringify(dataUser.data.data.user_found));

            //aqui va
            onUpdateUser(dataUser.data.data.user_found)
        }
    },[]);

    const userName = isUserLogged.first_name
    const userData  = [
        {header:'Nombre', value: isUserLogged.first_name},
        {header:'Apellidos', value: isUserLogged.fathers_last_name},
        {header:'Edad', value:'26'},
        {header:'Género', value:'Masculino'},
        {header:'Fecha de nacimiento', value:isUserLogged.birth_date},
      ];
    

    /* let localUserData = localStorage.getItem('dataUser')
    if(localUserData){
        localUserData=JSON.parse(localUserData)
        console.log(localUserData)
        //console.log('hola')
        onUpdateUser(localUserData)

        userName = localUserData.first_name
        userData =(
            <div className={classes.data}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography>Nombre: {localUserData.first_name}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography>Apellidos: {localUserData.fathers_last_name}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography>Email: {localUserData.email}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography>Fecha de nacimiento: {localUserData.birth_date} </Typography>
                    </Grid>
                </Grid>
            </div>)
    } */

    
    
    return(
        <div className={classes.root}>
            <div className={classes.subroot}>
                <Grid container spacing={4}>
                    <Grid item md={6} xs={12}>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <Typography variant='h2'>Bienvenido {userName} </Typography>
                            </Grid>
                            <Grid item xs>
                                <SearchBar />
                            </Grid>
                            <Grid item xs={12}>
                                <UserOp />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <UserData rows = {userData}/>
                    </Grid>
                </Grid>
            </div>
        </div>
    );

}

//redux : get the general state as a prop
const mapStateToProps = state => {
    return{
        isUserLogged : state.user
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onUpdateUser: (user) => dispatch({
            type: actionTypes.AUTH_UPDATE_USER, 
            user: user 
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserView);

/* return(
        <div className={classes.root}>
            <ToolbarUser name={name} email={email}/>
            <main className={classes.content}>
                <div className={classes.welcome}>
                    <Typography variant='h2'>Bienvenido </Typography>
                    
                </div>
                <Footer />      
            </main>
        </div>
    ); */