 import React from 'react';
 import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';

//imagenes
import salud from '../../../assets/icons/corazon.png'
import citas from '../../../assets/icons/estetoscopio.png'
import eventos from '../../../assets/icons/portapapeles.png'

const useStyles = makeStyles((theme) => ({
    root: {

    },
    op:{
        width:'100%',
        height:300,
        borderRadius:15,
        //backgroundColor:'red',
        margin:'12px 0px',
        position:'relative'
    },
    content:{
        position:'absolute',
        left:'50%',
        top:'50%',
        transform:'translate(-50%,-50%)',
        color:'white',
        textAlign:'center'
    },
    typo:{
        marginTop:16
    }
}));

const UserOp = (props) =>{

    const classes = useStyles();
    
    return(
        <Grid container spacing={2}>
            <Grid item md sm xs={12} >
                <div className={classes.op} style={{backgroundColor: '#d64933'}}>
                    <div className={classes.content}>
                        <img alt='' src={salud} />
                        <Typography className={classes.typo}>Salud</Typography>
                    </div>
                </div>
            </Grid>
            <Grid item md sm xs={12}>
                <div className={classes.op} style={{backgroundColor: '#1B4965'}}>
                    <div className={classes.content}>
                        <img alt='' src={citas} />
                        <Typography className={classes.typo}>Citas</Typography>
                    </div>
                </div>
            </Grid>
            <Grid item md sm xs={12}>
                <div className={classes.op} style={{backgroundColor: '#f3b61f'}}>
                    <div className={classes.content}>
                        <img alt='' src={eventos} />
                        <Typography className={classes.typo}>Eventos</Typography>
                    </div>
                </div>
            </Grid>
        </Grid>
    );

}
export default UserOp;