 import React from 'react';
 import { makeStyles } from '@material-ui/core/styles';
 import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Grid, Icon, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    mainroot:{
        height:'100%',
    },
    root: {
        backgroundColor:'white',
        borderRadius:15,
        //paddingTop:16,
        height:'92%',
        [theme.breakpoints.down('md')]: {
            height:'100%'
            //backgroundColor:'red'
        },
        
    },
    table:{
        width:'80%',
        margin:'auto',
        //height:'100%'
        
    },
    footer:{
        //height:'15%',
        //position:'absolute',
        //bottom:4,
        //left:'39%',
        marginTop:12,
        color:theme.palette.secondary.main,
        
    },
    tableCell:{
        color:theme.palette.secondary.main
    }
}));

const UserData = (props) =>{

    const classes = useStyles();

    const {rows} = props;

    /* const rows = [
        {header:'Nombre', value:'Cesar'},
        {header:'Apellidos', value:'Gonzalez'},
        {header:'Edad', value:'26'},
        {header:'Género', value:'Masculino'},
      ]; */
    
    return(
        
        <div className={classes.mainroot}>
            <div className={classes.root}>
            <Grid item xs={12}>
                <div className={classes.table}>
                    <Grid container justify='center' spacing={3}>
                        <Grid item>
                            <Typography variant='h4' color='secondary' style={{marginTop:16}}>Datos generales</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Table  aria-label="simple table">
                                <TableBody>
                                {rows.map((row) => (
                                    <TableRow key={row.header}>                      
                                    <TableCell component="th" scope="row" className={classes.tableCell}>
                                        {row.header}
                                    </TableCell>                                   
                                    <TableCell align="left">{row.value}</TableCell>
                                    </TableRow>
                                ))}
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </div>
            </Grid>
            </div>
            <div className={classes.footer}>
                <Grid container justify='center' spacing={3}>
                    <Grid item><Icon>west</Icon></Grid>
                    <Grid item><Typography>1/3</Typography></Grid>
                    <Grid item><Icon>east</Icon></Grid>
                </Grid>
            </div>       
        </div>
    );

}
export default UserData;