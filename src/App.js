import './App.css';

import { ThemeProvider } from '@material-ui/core/styles';
import theme from './theme';
import MainLayout from './layouts/MainLayout';

//redux
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './store/reducer'

const store = createStore(reducer);

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <MainLayout />
      </ThemeProvider>
    </Provider>
  );
}

export default App;
