//import { updateObject } from '../shared/utility'
import * as actionTypes from './actions'

const initialState = {
    user:{
        first_name: '',
        fathers_last_name:'',
        email:'',
        birth_date:''
    },
}

const reducer = (state = initialState, action) => {

    switch(action.type){
        /* case actionTypes.CHANGE_AUTH_STATUS:
            return updateObject(state, {isauth:action.isauth})
        case actionTypes.UPDATE_CART_ITEMS:
            return{
                ...state,
                cartItems:action.cartItems,  //pregunta las cartItems son cartItems del estado copiado?
            }
        case actionTypes.UPDATE_APOLLO_CLIENT:
            return{
                ...state,
                apolloClient:action.apolloClient,
            } */
        case actionTypes.AUTH_UPDATE_USER:
            return {
                ...state,
                user: action.user
            }
        default:
            return state;

    }
}

export default reducer;