import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid, Icon } from '@material-ui/core';

import ButtonMenu from './components/ButtonMenu';
import SearchBar from './components/SearchBar';
import Logo from './components/Logo';
import Login from '../Modals/Login';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '20px 0px',
        marginRight:18,
        width:'100%'
    },
    button:{
        color:theme.palette.primary.main,
        fontWeight:500,
        fontSize:16,
        marginTop:20,
        marginRight:16,
    },
    search:{
        marginTop:12
    },
    logo:{
        color:theme.palette.secondary.main,
        marginLeft:100,
        marginRight:60,
        marginTop:12
    },
    login:{
        marginTop:12
    }
}));

const Toolbar = (props) =>{

    const classes = useStyles();

    const salud=[{header:'Enfermedades A-Z', options: ['Acné', 'Artritis', 'Asma', 'Bio True']},
                 {header:'Procedimientos A-Z', options: ['Acné', 'Artritis', 'Asma', 'Bio True']},
                 {header:'Laboratorios A-Z', options: ['Acné', 'Artritis', 'Asma', 'Bio True']}];

    const medicamentos=[{header:'Medicinas (Alopáticos)', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},
                        {header:'Dermocósmeticos', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},
                        {header:'Suplementos y vitaminas', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']}];

    const bienestar=[{header:'Nutrición y ejercicios', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},
                     {header:'Sexualidad', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},
                     {header:'Piel y belleza', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']}];
    
    const familia=[{header:'Embarazo', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},
                    {header:'Guía de padres', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},
                    {header:'Adultos', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},
                    {header:'Mascotas', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']}];

    const noticias=[{header:'Noticias de salud', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},
                    {header:'Medirectorio', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},];

    const videos=[{header:'Entrevistas con expertos', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},
                  {header:'Entrevistas con pacientes', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']},
                  {header:'Show en vivo', options: ['Opti Free', 'Ao Sept', 'Renu', 'Bio True']}];
    
    return(
        <div className={classes.root}>
            <Grid container>
                <Grid item>
                    <div className={classes.logo}>
                    <Logo />
                    </div>
                </Grid> 
                <Grid item>
                    <ButtonMenu options={salud}>SALUD</ButtonMenu>
                </Grid> 
                <Grid item>
                    <ButtonMenu options={medicamentos}>MEDICAMENTOS</ButtonMenu>
                </Grid>
                <Grid item>
                    <ButtonMenu options={bienestar}>BIENESTAR</ButtonMenu>
                </Grid> 
                <Grid item>
                    <ButtonMenu options={familia}>FAMILIA</ButtonMenu>
                </Grid> 
                <Grid item>
                    <ButtonMenu options={noticias}>NOTICIAS Y EXPERTOS</ButtonMenu>
                </Grid>
                <Grid item>
                    <ButtonMenu options={videos}>VIDEOS</ButtonMenu>
                </Grid> 
                <Grid item>
                    <Button className={classes.button}>PREGUNTAS</Button>
                </Grid>
                <Grid item>
                    <div className={classes.login}>
                        <Login  />
                    </div>
                </Grid>
                {/* <Grid item>
                    <div className={classes.search}>
                    <SearchBar />
                    </div>
                </Grid> */}
            </Grid>
        </div>
    );

}
export default Toolbar;