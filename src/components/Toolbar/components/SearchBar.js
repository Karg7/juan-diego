import React from 'react';

import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { grey } from '@material-ui/core/colors';

 const useStyles = makeStyles((theme) => ({
    search: {
        position: 'relative',
        borderRadius: 40,
        backgroundColor: 'white',
        padding:'8px 16px',
        boxShadow:'0 10px 24px 0 rgba(82, 91, 115, .12)',
        [theme.breakpoints.down('sm')]: {
          display:'none'
        },
        
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color:'grey'
    },
    inputRoot: {
        //color: 'inherit',
        color: 'black',
        width:'100%',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
          width: '20ch',
        },
    },
    icon:{
      color:grey[500],
      marginTop:4,
    }
    

  }));

const SearchBar = (props) =>{

    const classes = useStyles();

    return (
        <div className={classes.search}>
          <Grid container alignItems='center' spacing={1} >
            <Grid item >
              <SearchIcon className={classes.icon}/>
            </Grid>
            <Grid item xs>
              <InputBase
              placeholder="Buscar"
              fullWidth
              inputProps={{ 'aria-label': 'search' }}
              />
            </Grid>
          </Grid>
        </div>
    );
}
 
export default SearchBar;