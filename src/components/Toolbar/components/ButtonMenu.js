import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

//menu
import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Button, Grid, Icon, Typography } from '@material-ui/core';

//const sections =[{label:'Quiero registrarme', src:closeEye}, {label:'Ya tengo cuenta', src: openEye},];

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
    borderRadius:16
  },
  })((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      //backgroundColor: theme.palette.primary.main,
      backgroundColor: '#90a4ae',
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);


const useStyles = makeStyles((theme) => ({
    root: {
        color:theme.palette.primary.main,
        fontWeight:500,
        fontSize:16,
        marginTop:20,
        marginRight:16,
    },
    menu:{
      padding:32,
      //backgroundColor:'red'
    },
    imgIcon:{
        width:30,
        height:30,
    },
    menuItem:{
      //width:150,
      padding:'4px 0px',
      //marginLeft:8,
      color:'#455a64',
      fontWeight:500,
    },
    menuItemHeader:{
      width:180,
      paddingLeft:16,
      textTransform:'none',
    }
    
}));

const ButtonMenu = (props) => {

    const classes = useStyles();

    const {options} = props;

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
      setAnchorEl(null);
    };

    
    return(
        <div>
            <Button
            className={classes.root}
            onClick={handleClick}
            >
            <Grid container>
                <Grid item>{props.children}</Grid>
                <Grid item><Icon>expand_more</Icon></Grid>
            </Grid>
            </Button>
            <StyledMenu
                id="customized-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
               
            >
              <Grid container >
              {options.map((item, index) => {
                return(
                  <Grid item key={index}>
                    <Grid container direction='column' className={classes.menu}>
                      <Grid item>
                        <Typography  className={classes.menuItemHeader}>{item['header']}</Typography>
                      </Grid>
                      {item['options'].map((op, i) => {
                          return(
                            <Grid item key={i}>
                              <StyledMenuItem>
                                <Typography variant='body2' className={classes.menuItem}>{op}</Typography>
                              </StyledMenuItem>
                            </Grid>
                          )
                        })}
                    </Grid>
                  </Grid>
                )
              })}
              </Grid>
            </StyledMenu>
        </div>
        
    );

}
export default ButtonMenu;