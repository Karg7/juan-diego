export const formData = {
    username:{
      value: '',
      error: false,
      isVisited: false,
      isRequired: true,
      isValid: false,
      config:{
        id:'username',
        type:'text',
        label:'Email',
        placeholder:'Ingrese su usuario',
        helper:'No es un email válido',
        icon:{name:'person', position:'start'}
      },
      rules:{
        type:'email',
      }
    },
    password:{
      value: '',
      error: false,
      isVisited: false,
      isRequired: true,
      isValid: false,
      config:{
        id:'password',
        type:'password',
        placeholder:'Ingrese su contraseña',
        label:'Contraseña',
        icon:{name:'lock', position:'start'}
      },
      rules:{
        type:'distance',
      }
    },
  }