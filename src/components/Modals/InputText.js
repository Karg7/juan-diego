 import React from 'react';
 import { makeStyles } from '@material-ui/core/styles';
 import {ruleValidation} from './costumFunctions';
import { Icon, InputAdornment, TextField } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {

    },
}));

const InputText = (props) =>{

    const classes = useStyles();

    const {onChange, data, onKeyPress, disabled} = props
    const {value, isVisited, isValid, isRequired} = props.data
    const {id, type,label,placeholder,fullWidth,icon, multiline, rows, helper} = props.data.config

    const onInnerChange = event => {
        let response = event.target.value;
        let temp = {...data};
        if(temp.config.type === 'number'){
          response = temp.config.float ? Number(response).toString() : parseInt(response).toString()
        }
        temp.value = response;
        temp.isValid = ruleValidation(temp.value, temp.rules, temp.value2);
        temp.isVisited = true;
        onChange(temp);
      }

      const error = isVisited && !isValid;

    let inputaddornment = null;
    if(icon){
        //console.log(icon)
        inputaddornment = 
        <InputAdornment position='start'>
            <Icon color='primary'>{icon.name}</Icon>
        </InputAdornment>
    }
    
    return(<TextField 
        className={classes.textfield} 
        label={label}
        disabled={disabled}
        error={error}
        value={value !== undefined && value !== null ? value : ''}
        onChange={onInnerChange}
        type={type}
        helperText={error ? helper : null}
        InputProps={{
            endAdornment: inputaddornment
        }}
        /> );

}
export default InputText;