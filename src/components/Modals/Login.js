import React, {useState} from 'react';
import {Redirect} from "react-router-dom";
import moment from 'moment';
//react redux
import {connect} from 'react-redux'

import {mainServer} from '../../variables/config';
import {formData} from './data'

import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Button, CircularProgress, Grid, Icon,  Modal, Popover, Typography } from '@material-ui/core';
import InputText from './InputText';


 /* function rand() {
    return Math.round(Math.random() * 20) - 10;
  } */
  
  function getModalStyle() {
    const top = 50 
    const left = 50
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
  }

  function getModalStyleClose() {
    
    return {
      top: '0',
      left: '0',
      //transform: `translate(-${top}%, -${left}%)`,
    };
  }

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'absolute',
        width: 300,
        backgroundColor: 'white',
        borderRadius:15,
        //boxShadow: theme.shadows[5],
        padding: '60px 40px'
    },
    textfield:{
        width:'100%'
    },
    button:{
        padding:'8px 32px',
        color:'white',
        backgroundColor:theme.palette.primary.main,
        borderRadius:40,
        marginRight:12,
        marginBottom:20,
        '&:hover':{
            backgroundColor:theme.palette.primary.dark
        }
    },
    buttonContainer:{
        textAlign:'center'
    },
    good:{
        color:'green'
    },
    error:{
        color:'red'
    },
    //classes for logged user
    avatar:{
        color:'white',
        backgroundColor:theme.palette.primary.main
    },
    avartarLarge:{
        width: theme.spacing(7),
        height: theme.spacing(7),
        backgroundColor:'white',
        color:theme.palette.primary.main,
        border:'1px solid gray'
    },
    pop:{
        padding:8,
        width:300,
        borderRadius:30,
        boxShadow:'0 10px 24px 0 rgba(82, 91, 115, .12)',
        
    },
    loginUser:{
        paddingTop:16,
        paddingLeft:16
    }
}));

const Login = (props) =>{

    const classes = useStyles();

    const [modalStyle] = useState(getModalStyle);
    const [modalStyleClose] = useState(getModalStyleClose);
    const [open, setOpen] = useState(false);

    const [data, setData] = useState(JSON.parse(JSON.stringify(formData)));

    const [isValid, setIsValid]= useState(false);
    const [message, setMessage] = useState(' ');
    const [loading, setLoading] = useState(false);

    //cerrar sesion
    const [sesion, setSesion] = useState(false);

    //popover de usuario logeado
    const [anchorEl, setAnchorEl] = useState(null);
    
    //--------MODAL STATUS-------------//
    const handleOpen = () => {setOpen(true)};
    const handleClose = () => {setOpen(false)};

    //-------POPOVER STATUS---------------//
    const handleClickPop = (event) => {setAnchorEl(event.currentTarget)};
    const handleClosePop = () => {setAnchorEl(null)};

    //-------VALIDATION------------//
    const onInputChange = (dta) => {

        const id = dta.config.id;
        let temp = {...data};
        temp[id] = {...dta};
        const isValidForm = validationForm(temp);
        setData({...temp});
        setIsValid(isValidForm)
    } 

    const validationForm = (data) => {
        let isValid = true;
        Object.keys(data).forEach((item) => {
          if(data[item].isRequired && !data[item].isValid){
            isValid = false;
          }
        })
        return isValid;
    }

    const loginHandler = (data) =>{
        setLoading(true)
        mainServer.instance.defaults.headers.common['Authorization'] = 'Basic ' + btoa(mainServer.credentials.user + ':' + mainServer.credentials.password);

        mainServer.instance.post('/login', data)
            //
            .then( response => {
                setLoading(false)
                console.log(response)
                setMessage('Ahora estas logeado hermos@ :)')
                localStorage.setItem('localData',JSON.stringify(response.data.data));
                setSesion(true)
                setOpen(false)
                console.log(sesion)
            })
            .catch(
                error => {
                    switch (error.response.status) { 
                        case 401:
                          setLoading(false)
                          setMessage('Se te olvido tu contraseña :(')
                          break;
                        case 404:
                          setLoading(false)
                          setMessage('El usuario no existe')
                          break;
                        default:
                          break;
                      }
                }
            )

        
            
    }

    const sesionHandler = () => {
        setSesion(false)
        setOpen(false)
        setMessage('')
        //data.username.value=''
        //data.password.value=''
        localStorage.removeItem('localData');
    }

    const loginData = {
        email: data.username.value,
        password: data.password.value
    } 

 
    let alert = null

    if (message === 'Ahora estas logeado hermos@ :)'){
        alert = (<div className={classes.good}>
                    <Grid container>
                        <Grid item><Icon >check_circle_outline</Icon></Grid>
                        <Grid item><Typography>{message}</Typography></Grid>
                    </Grid>
                </div>)
    }

    if (message === 'Se te olvido tu contraseña :('){
        alert = (<div className={classes.error}>
                    <Grid container>
                        <Grid item><Icon >error_outline</Icon></Grid>
                        <Grid item><Typography>{message}</Typography></Grid>
                    </Grid>
                </div>)
    }

    if (message === 'El usuario no existe'){
        alert = (<div className={classes.error}>
                    <Grid container>
                        <Grid item><Icon >error_outline</Icon></Grid>
                        <Grid item><Typography>{message}</Typography></Grid>
                    </Grid>
                </div>)
    }

    let spinner = null;

    if (loading){
        spinner= <CircularProgress />
    }

    /* let temp = localStorage.getItem('localData');
    if(temp){
      temp = JSON.parse(temp);
      let currentTime = moment().unix();
      let tokenTime = temp.exp;
      //si faltan 15 min para que expire el token
      //y ademas el usuario esta moviendo el mouse, => se reautentica
      if (currentTime === (tokenTime - 4011538200.5)){
        mainServer.instance.defaults.headers.common['Authorization'] = temp.token
      }
      //si el token expiro y el usuario no ha movido el mouse en todo ese tiempo
      //se cierra la sesion
      if(currentTime < tokenTime){
        console.log('activo holis')
        }else{
            console.log('inactivo')
        }   
    }else{
        console.log('no ha iniciado sesion')
    } */
    
    
    let content = (
        <div>
            <Button type="button" onClick={handleOpen}>
                <Icon color='primary'>person</Icon>
            </Button>
            <Modal
                //disablePortal
                disableEnforceFocus
                disableAutoFocus={true}
                open={open}
                onClose={handleClose}
                //aria-labelledby="simple-modal-title"
                //aria-describedby="simple-modal-description"
            >
            <div style={modalStyle} className={classes.root}>
            <Grid container spacing={3} >
                <Grid item xs={12}>
                    <Typography variant='h4' color='primary'>Login</Typography>
                </Grid>
                <Grid item xs={12}>
                    <InputText 
                    data={data.username}
                    onChange={onInputChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InputText 
                    data={data.password}
                    onChange={onInputChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <div className={classes.buttonContainer}>
                    <Button className={classes.button} onClick={()=>{loginHandler(loginData)}}>Login</Button>
                    {spinner}  
                    </div>
                </Grid>
                <Grid item xs={12}>
                    {alert}
                </Grid>
            </Grid>
            </div>
            </Modal>
            </div>
    );

    const openPop = Boolean(anchorEl);
    const id = openPop ? 'simple-popover' : undefined;

    if (sesion) {
       // console.log(sesion)
        content=(
            <div>
            <Button type="button" onClick={handleClickPop}>
                <Avatar alt={props.isUserLogged.first_name}  src="/broken-image.jpg" className={classes.avatar} />
            </Button>
            <Popover
            id={id}
            open={openPop}
            anchorEl={anchorEl}
            onClose={handleClosePop}
            anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
            }}
            transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
            }}
            >
                <div className={classes.pop}>  
                    <Grid container spacing={2}>
                        <div className={classes.loginUser}>
                        <Grid item xs={12}>
                            <Grid container spacing={2}>
                                <Grid item xs={3}>
                                    <Avatar alt={props.isUserLogged.first_name} src="/broken-image.jpg" className={classes.avartarLarge} />
                                </Grid>
                                <Grid item xs={9}>
                                    <Typography>{props.isUserLogged.first_name + ' ' + props.isUserLogged.fathers_last_name}</Typography>
                                    <Typography>{props.isUserLogged.email}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        </div>
                        <Grid item xs={12}>
                            <Button color='primary' onClick={sesionHandler}>Cerrar Sesión</Button>
                        </Grid>
                    </Grid>
                </div>
            </Popover>
            </div> 
        )
    }

    
    return(
        <div>
            {content}
            {sesion && <Redirect to='/user' />}
            {!sesion && <Redirect to='/' />}
        </div>
    );

}

//redux : get the general state as a prop
const mapStateToProps = state => {
    return{
        isUserLogged : state.user
    }
}

export default connect(mapStateToProps)(Login);

//export default Login;