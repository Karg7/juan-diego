import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Grid, Typography } from '@material-ui/core';

import logo from '../../../assets/logo.JPG';

const useStyles = makeStyles((theme) => ({
    root: {

    },
    avatar:{
        width:50,
        height:50,
        margin:8,
        //marginTop:12
    }
}));

const Logo = (props) =>{

    const classes = useStyles();
    
    return(
        <Grid container>
            <Grid item>
                <Avatar className={classes.avatar} alt="Remy Sharp" src={logo} />
            </Grid>
            <Grid item>
                <Typography variant='h5'>Doctor</Typography>
                <Typography variant='h5'>Juan Diego</Typography>
            </Grid>
        </Grid>
    );

}
export default Logo;