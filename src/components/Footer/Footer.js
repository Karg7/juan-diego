import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, IconButton, Typography } from '@material-ui/core';

import Logo from '../Toolbar/components/Logo';

//redes sociales
import fb from '../../assets/redes sociales/facebook.png';
import insta from '../../assets/redes sociales/instagram.png';
import twiter from '../../assets/redes sociales/twiter.png';
import youtube from '../../assets/redes sociales/youtube.png';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop:40,
        backgroundColor: theme.palette.primary.main,
        padding:'30px 40px',
        color:'white'
    },
    subroot:{
        width:'85%',
        margin:'auto'
    },
    line:{
        width:'100%',
        height:1,
        backgroundColor:'white',
        marginRight:30,
    },
    social:{
        width:40,
    }
}));

const Footer = (props) =>{

    const classes = useStyles();
 
    return(
        <div className={classes.root}>
            <div className={classes.subroot}>
                <Grid container spacing={2} >
                    <Grid item md={8}>
                        <Logo />
                    </Grid>
                    <Grid item md={4}>
                        <Grid container justify='flex-end'  >
                            <Grid item>
                                <IconButton><img className={classes.social} src={fb} alt=''/></IconButton>
                            </Grid>
                            <Grid item>
                                <IconButton><img className={classes.social} src={insta} alt=''/></IconButton>
                            </Grid>
                            <Grid item>
                                <IconButton><img className={classes.social} src={twiter} alt=''/></IconButton>  
                            </Grid>
                            <Grid item>
                                <IconButton><img className={classes.social} src={youtube} alt=''/></IconButton>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <div className={classes.line}></div>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography>COPYRIGH klsjdfaskjfldñsa</Typography>
                    </Grid>
                </Grid>
            </div>
        </div>
    );

}
export default Footer;