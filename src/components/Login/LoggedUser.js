 import React, {useState} from 'react';
 import {Redirect, Link} from "react-router-dom";
 import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Button, Grid, Popover, Typography} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {

    },
    avatar:{
        color:'white',
        backgroundColor:theme.palette.primary.main
    },
    avartarLarge:{
        width: theme.spacing(7),
        height: theme.spacing(7),
        backgroundColor:'white',
        color:theme.palette.primary.main,
        border:'1px solid gray'
    },
    pop:{
        padding:8,
        width:300,
        borderRadius:30,
        boxShadow:'0 10px 24px 0 rgba(82, 91, 115, .12)',
        
    }
}));

const LoggedUser = (props) =>{

    const classes = useStyles();

    const [anchorEl, setAnchorEl] = useState(null);
    
    const [userData, setUserData] = useState(false)

    const [sesion, setSesion] =useState(false)

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const closeSesionHandler = () =>{
        localStorage.removeItem('localData')
        setSesion(true)
        //console.log('hola')
       /*  return(
            <Link to='/hola' />
        )   */
    }

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;
    
    return(
        <div>
            <Button type="button" onClick={handleClick}>
                <Avatar alt={props.name}  src="/broken-image.jpg" className={classes.avatar} />
            </Button>
            <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
            }}
            transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
            }}
            >
                <div className={classes.pop}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Grid container>
                                <Grid item xs={3}>
                                    <Avatar alt={props.name} src="/broken-image.jpg" className={classes.avartarLarge} />
                                </Grid>
                                <Grid item xs={9}>
                                    <Typography>{props.name}</Typography>
                                    <Typography>{props.email}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            
                            <Button color='primary' onClick={closeSesionHandler}>Cerrar Sesión</Button>
                            
                        </Grid>
                    </Grid>
                </div>
            </Popover>
            {sesion && <Redirect to='/' />}
        </div>
    );

}
export default LoggedUser;