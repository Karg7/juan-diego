import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
   root: {
       borderRadius: 15,
       boxShadow:'0 10px 24px 0 rgba(82, 91, 115, .12)',
       backgroundColor: 'white',
      
   },
}));

const SimpleCard = (props) =>{

   const classes = useStyles();
   
   return(
       <div className={classes.root}>
           {props.children}
       </div>
   );

}
export default SimpleCard;